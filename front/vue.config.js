const path = require('path')

module.exports = {
    outputDir: path.resolve(__dirname, '../priv/static'),
    publicPath: '/dist/',
    configureWebpack: {
      resolve: {
        alias: require('./aliases.config').webpack
      },
      context: path.resolve(__dirname, './'),
      entry: {
        app: './src/main.js'
      },
      output: {
        filename: 'js/app.js'
      },
      node: {
        // prevent webpack from injecting useless setImmediate polyfill because Vue
        // source contains it (although only uses it if it's native).
        setImmediate: false,
        // prevent webpack from injecting mocks to Node native modules
        // that does not make sense for the client
        dgram: 'empty',
        fs: 'empty',
        net: 'empty',
        tls: 'empty',
        child_process: 'empty'
      }
    },
    css: {
      // Enable CSS source maps.
      sourceMap: true
    }
  }