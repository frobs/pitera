import Vue from 'vue'
import Router from 'vue-router'
import AdminLayout from '@/components/layouts/AdminLayout.vue'
import Home from '@/views/Home.vue'
import Inventory from '@/views/inventory/Home.vue'
import Materials from '@/views/inventory/materials/MaterialList.vue'
import StockList from '@/views/inventory/stock/StockList.vue'
import StockNew from '@/views/inventory/stock/StockNew.vue'
import StockUpdate from '@/views/inventory/stock/StockUpdate.vue'
import MaterialsNew from '@/views/inventory/materials/MaterialNew.vue'
import MaterialsUpdate from '@/views/inventory/materials/MaterialUpdate.vue'
import Products from '@/views/inventory/products/ProductList.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: AdminLayout,
      children: [
        {
          path: '/',
          name: 'Home',
          component: Home
        },
        {
          path: '/inventory',
          name: 'Inventory',
          component: Inventory,
          children: [
            {
              path: '/material',
              name: 'Materials',
              component: Materials
            },
            {
              path: '/material/new',
              name: 'MaterialsNew',
              component: MaterialsNew
            },
            {
              path: '/material/:id/update',
              name: 'MaterialsUpdate',
              component: MaterialsUpdate
            },
            {
              path: '/material/:id/stock',
              name: 'StockList',
              component: StockList
            },
            {
              path: '/material/:id/stock/new',
              name: 'StockNew',
              component: StockNew
            },
            {
              path: '/material/:material_id/stock/:id',
              name: 'StockUpdate',
              component: StockUpdate
            },
            {
              path: '/product',
              name: 'Products',
              component: Products
            }
          ]
        }
      ]
    },
    
  ]
})
