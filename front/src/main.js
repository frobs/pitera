import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import VueResource from 'vue-resource'
import moment from 'moment'
import Notifications from 'vue-notification'
import VueResourceCaseConverter from 'vue-resource-case-converter'


Vue.config.productionTip = false

Vue.use(VueResource)
Vue.use(VueResourceCaseConverter)
Vue.use(Notifications)

Vue.filter('currency', function (value) {
  let valueAsArray = value.toString().split(".")
  let int = valueAsArray[0]
  let decimal = valueAsArray.slice(1).join("")
  let rounded =  +(Math.round(parseFloat(int + "." + decimal) + "e+4")  + "e-4");
  return rounded.toString().replace('.', ',') + ' €'
})

Vue.filter('decimals', function (value) {
  let valueAsArray = value.toString().split(".")
  let int = valueAsArray[0]
  let decimal = valueAsArray.slice(1).join("")
  let rounded =  +(Math.round(parseFloat(int + "." + decimal) + "e+4")  + "e-4");
  return rounded.toString().replace('.', ',')
})

Vue.filter('date', function (date) {
  return moment(date).format('DD-MM-YYYY')
})

Vue.filter('snakeCase', function (string) {
  return string.replace(/[\w]([A-Z])/g, function(m) {
    return m[0] + "_" + m[1];
  }).toLowerCase();
})




new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
