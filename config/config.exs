# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :pitera,
  ecto_repos: [Pitera.Repo]

# Configures the endpoint
config :pitera, PiteraWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "GSJ84sCjOnqhjnZwq58mmoDlo5otJQERiZg5klwJWqkhKM703f8LR9zwXTRNVH/D",
  render_errors: [view: PiteraWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Pitera.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

#Pow
config :pitera, :pow,
  user: Pitera.Users.User,
  repo: Pitera.Repo,
  extensions: [PowResetPassword, PowPersistentSession],
  controller_callbacks: Pow.Extension.Phoenix.ControllerCallbacks,
  web_module: PiteraWeb,
  mailer_backend: PiteraWeb.PowMailer,
  web_mailer_module: PiteraWeb,
  cache_store_backend: PiteraWeb.PowRedisCache

#Swoosh
config :pitera, PiteraWeb.PowMailer,
  adapter: Swoosh.Adapters.Postmark,
  api_key: "05e93a0e-a9e7-48ad-bc9d-a22d287acc622"


config :phoenix, :format_encoders,
       json: Jason

config :phoenix, :json_library, Jason

config :postgrex, :json_library, Jason

config :mime, :types, %{
  "application/json" => ["json"]
}
