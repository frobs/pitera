use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :pitera, PiteraWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger,
  backends: [:console],
  compile_time_purge_matching: [
    [level_lower_than: :info]
  ]

# Configure your database
config :pitera, Pitera.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "1221",
  database: "pitera_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
