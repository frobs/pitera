defmodule NumberHelperTest do
  @moduledoc false
  alias Pitera.Helpers.NumberHelper
  use ExUnit.Case

  describe "NumberHelper.to_exact_integer" do
    test "convert float to integer 5 decimals to integer (down round)" do
      assert NumberHelper.to_exact_integer(12.45532) == 124553
    end
    test "convert float to integer 5 decimals to integer (up round)" do
      assert NumberHelper.to_exact_integer(12.45536) == 124554
    end
  end

  describe "NumberHelper.to_exact_float" do
    test "convert float to integer 5 decimals to integer (down round)" do
      assert NumberHelper.to_exact_float(124555) == 12.4555
    end
  end

  describe "NumberHelper.round_to_four_decimals" do
    test "round down" do
      assert NumberHelper.round_to_four_decimals(12.34521) == 12.3452
    end

    test "round down with 5 decimals" do
      assert NumberHelper.round_to_four_decimals(12.34525) == 12.3453
    end

    test "round up with 5 decimals" do
      assert NumberHelper.round_to_four_decimals(12.34525) == 12.3453
    end
  end

  describe "NumberHelper.multiply" do
    test "multiply 12.0 and 3.0" do
      assert NumberHelper.multiply(12.0, 3.0) == 36.0
    end

    test "multiply 12.00001 and 3.00001" do
      assert NumberHelper.multiply(12.00001, 3.00001) == 36.0
    end

    test "multiply 12 and 3" do
      assert NumberHelper.multiply(12, 3) == 36.0
    end

    test "multiply 12.0 and 3" do
      assert NumberHelper.multiply(12.0, 3) == 36.0
    end
  end

  describe "NumberHelper.divide" do
    test "divide 12.0 and 3.0" do
      assert NumberHelper.divide(12.035, 2.3145) == 5.1998
    end

    test "divide 12.00001 and 3.00001" do
      assert NumberHelper.divide(12.03501, 2.31451) == 5.1998
    end

    test "divide 12 and 3" do
      assert NumberHelper.divide(12, 3) == 4.0
    end

    test "divide 12.0 and 3" do
      assert NumberHelper.divide(12.0350, 3.2) == 3.7609
    end
  end

end
