defmodule Pitera.InventoriesTest do
  use Pitera.DataCase

  alias Pitera.InventoriesFixture
  alias Pitera.Inventories.MaterialService

  describe "materials" do

    test "list_materials/1 returns all materials" do
    {:ok, materials} = MaterialService.list(1, %{})
      assert length(materials.entries) >= 2
    end

    test "get_material/2 returns the material with given id" do
      assert {:ok, material} = MaterialService.get(1, 1)
    end

    test "convert input to model" do
      assert MaterialService.input_to_model(InventoriesFixture.get_input_struct_material(), 1) == InventoriesFixture.get_material_model()
    end

    test "create_material/1 with valid data creates a material" do
      assert {:ok, material} = MaterialService.create(1, InventoriesFixture.get_input_struct_material(%{:name => "pollito"}))
      assert material.annotation == "Ricas ricas"
      assert material.name == "pollito"
      assert length(Enum.filter(material.properties, fn x -> x.name == "" end)) == 0
      assert material.sku == "233Jk332555345"
    end

    test "create_material/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = MaterialService.create(nil, InventoriesFixture.get_input_struct_material())
    end

    test "update_material/3 with valid data updates the material" do
      {:ok, old_material} = MaterialService.create(1, InventoriesFixture.get_input_struct_material(%{:name => "pollito"}))
      assert {:ok, material} = MaterialService.update(1, old_material.id, InventoriesFixture.get_input_struct_material(%{:annotation =>  "descripcion modificada"}))
      assert material.annotation == "descripcion modificada"
    end

    test "update_material/3 with invalid data returns error changeset" do
      {:ok, material} = MaterialService.get(1, 1)
      assert {:error, %Ecto.Changeset{}} = MaterialService.update(1, material.id, InventoriesFixture.get_input_struct_material(%{:name => nil}))
      assert {:ok, material} == MaterialService.get(1, material.id)
    end

    #test "delete_material/1 deletes the material" do
     # {:ok, material} = MaterialService.create(InventoriesFixture.get_input_struct_material(%{}), 1)
     # assert {:ok, %MaterialModel{}} = Inventories.delete_material(material, 1)
     # assert {:error, nil} =  Inventories.get_material(material.id, 1)
   # end

  end
end
