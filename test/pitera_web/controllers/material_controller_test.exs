defmodule PiteraWeb.MaterialControllerTest do
  use PiteraWeb.ConnCase

  alias Pitera.Inventories.MaterialService
  alias Pitera.Inventories.Inventories
  alias Pitera.InventoriesFixture

  @create_attrs InventoriesFixture.get_input_material(%{"annotation" => "some description", "name" => "some name", "properties" => [], "sku" => "some sku"})
  @create_input InventoriesFixture.get_input_struct_material(%{"annotation" => "some description", "name" => "some name", "properties" => [], "sku" => "some sku"})
  @update_attrs InventoriesFixture.get_input_material(%{"annotation" => "some updated description", "name" => "some updated name", "properties" => [], "sku" => "some updated sku"})
  @invalid_attrs InventoriesFixture.get_input_material(%{"annotation" => nil, "name" => nil, "properties" => nil, "sku" => nil})

  def fixture(:material) do
    {:ok, material} = MaterialService.create(1, @create_attrs)
    material
  end

  setup %{conn: conn} do
    user = %Pitera.Users.User{name: "Test User", password: "1234567890", email: "test@inventario.app", id: 1, default_account_id: 1}
    conn = conn |> put_req_header("accept", "application/json") |> assign(:current_user, user) |> assign(:business_id, 1)
    {:ok, conn: conn}

  end

  describe "index" do
    test "lists all materials", %{conn: conn} do
      conn = get conn, material_path(conn, :index),params: %{}
      result = json_response(conn, 200)["data"]
      assert is_list(result)
      assert length(result) >= 2
    end

    test "lists all materials with order", %{conn: conn} do
      conn = get conn, material_path(conn, :index), %{"page" => "1", "page_size" => "10", "sort_by" => ["total_units", "average_unit_price"], "sort_desc" => ["true", "false"]}
      result = json_response(conn, 200)["data"]
      assert is_list(result)
      assert result |> Enum.at(0) |> Map.get("id") == 2
      assert result |> Enum.at(1) |> Map.get("id") == 1
      assert length(result) >= 2
    end


  end

  describe "create material" do
    test "renders material when data is valid", %{conn: conn} do
      created_conn = post conn, material_path(conn, :create), @create_attrs
      assert %{"id" => id} = json_response(created_conn, 201)["data"]

      get_conn = get conn, material_path(conn, :show, id)
      assert %{
        "id" => id,
        "annotation" => "some description",
        "name" => "some name",
        "properties" => [],
        "sku" => "some sku"} = json_response(get_conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, material_path(conn, :create), material: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update material" do

    test "renders material when data is valid", %{conn: conn} do
      {:ok, material_to_update} = MaterialService.create(1, @create_input)
      _updated_conn = put conn, material_path(conn, :update, material_to_update.id), @update_attrs

      get_conn = get conn, material_path(conn, :show, material_to_update.id)
      assert %{
        "annotation" => "some updated description",
        "name" => "some updated name",
        "properties" => [],
        "sku" => "some updated sku"} = json_response(get_conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = put conn, material_path(conn, :update, 1), @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete material" do
    test "deletes chosen material", %{conn: conn} do
      {:ok, material_to_remove} = MaterialService.create(1, @create_input)
      remove_conn = delete conn, material_path(conn, :delete, material_to_remove.id)
      assert response(remove_conn, 204)
      get_conn = get conn, material_path(conn, :show, material_to_remove.id)
      assert json_response(get_conn, 404)
    end
  end
end
