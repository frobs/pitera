defmodule Pitera.InventoriesFixture do
  @moduledoc false
  alias Pitera.Inventories.MaterialModel
  alias Pitera.Inventories.StockModel
  alias Pitera.Inventories.AddedCostModel
  alias Pitera.Inventories.Inventories
  alias PiteraWeb.Inputs.MaterialInput
  alias PiteraWeb.Inputs.AddedCostInput
  alias PiteraWeb.Inputs.StockInput
  alias Pitera.Helpers.DateHelper

  def get_input_material(another_map \\ %{}) do
    Map.merge(%{
      "stock" => %{
        "added_costs" => [
          %{"description" => "transporte", "price" => 521.2154},
          %{"description" => "montaje", "price" => 100.0001}
        ],
        "acquisition_date" => "2018-12-07",
        "unit_price" => 1.043,
        "units" => 4.0
      },
      "annotation" => "Ricas ricas",
      "name" => "Patatas fritas",
      "properties" => [
        %{"name" => "sabor","value" => "huevo frito"},
        %{"name" => "peso","value" => "500 gr"}
      ],
      "sku" => "233Jk332555345",

    }, another_map)
  end

  def get_input_struct_material(another_map \\ %{}) do
    Map.merge(%MaterialInput{
      stock: %StockInput{
        added_costs: [
          %AddedCostInput{description: "transporte", price: 521.2154},
          %AddedCostInput{description: "montaje", price: 100.0001}
        ],
        acquisition_date:  "2018-12-07",
        unit_price: 1.043,
        units: 4.0
      },
      annotation: "Ricas ricas",
      name: "Patatas fritas",
      properties: %{
        "sabor" => "huevo frito",
        "peso" => "500 gr"
      },
      sku: "233Jk332555345",
    }, another_map)
  end

  def get_material_model(attrs \\ %{}) do
    date = DateHelper.string_to_date("2018-12-07")
    Map.merge(%MaterialModel{
      name: "Patatas fritas",
      sku: "233Jk332555345",
      annotation: "Ricas ricas",
      total_units: 4.0,
      average_unit_price: 156.3469,
      total_price: 625.3875,
      properties: %{
        "sabor" : "huevo frito",
        "peso" : "500 gr"
                  },
                                             },
      business_id: 1,
      stock: [
        %StockModel{
          units: 4.0,
          unit_price: 1.043,
          total_added_cost: 621.2155,
          total_price: 625.3875,
          total_unit_price: 156.3469,
          acquisition_date: date,
          business_id: 1,
          added_costs: [
            %AddedCostModel{concept: "transporte", total: 521.2154},
            %AddedCostModel{concept: "montaje", total: 100.0001}
          ]
        }
      ]
    }, attrs)
  end

end
