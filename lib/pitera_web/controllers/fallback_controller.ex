defmodule PiteraWeb.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use PiteraWeb, :controller

  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(PiteraWeb.ChangesetView)
    |> render("error.json", changeset: changeset)
  end

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> render(PiteraWeb.ErrorView, :"404")
  end

  def call(conn, {:error, :internal_service_error}) do
    conn
    |> put_status(:internal_server_error)
    |> render(PiteraWeb.ErrorView, :"500")
  end

  def call(conn, {:invalid_entity, validation_error}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(PiteraWeb.ValidationErrorView)
    |> render("index.json", validation_error: validation_error)
  end

  def call(conn, {:input_contract_invalid, validation_error}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(PiteraWeb.ValidationErrorView)
    |> render("index.json", validation_error: validation_error)
  end
end
