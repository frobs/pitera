defmodule PiteraWeb.PageController do
  use PiteraWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end

end
