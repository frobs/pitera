defmodule PiteraWeb.MaterialController do
  use PiteraWeb, :controller

  alias Pitera.Inventories.Inventories
  alias Pitera.Inventories.MaterialModel
  alias Pitera.Inventories.StockModel
  alias PiteraWeb.Inputs.MaterialInput
  alias PiteraWeb.Inputs.StockInput
  alias PiteraWeb.StockView
  alias PiteraWeb.Contracts.MaterialContract
  alias PiteraWeb.Contracts.StockContract
  alias Pitera.Inventories.MaterialService
  alias Pitera.Inventories.StockService

  action_fallback PiteraWeb.FallbackController

  def index(conn, params) do
    with {:ok, paginated_materials} <- MaterialService.list(conn.assigns.business_id, params) do
      conn
      |> put_view(PiteraWeb.PaginatedView)
      |> render("index.json", %{
        paginated_results: paginated_materials,
        module: PiteraWeb.MaterialView,
        template: "material.json"
      })
    end
  end

  @spec get_stock_list(Plug.Conn.t(), map) :: Plug.Conn.t()
  def get_stock_list(conn, params) do
    id = Integer.parse(Map.get(params, "id")) |> elem(0)
    with {:ok, stocks} <- MaterialService.list_stock(conn.assigns.business_id, id) do
      conn
      |> put_view(StockView)
      |> render("stock_table.json", stocks: stocks)
    end
  end

  @spec get_stock_by_id(Plug.Conn.t(), map) :: Plug.Conn.t()
  def get_stock_by_id(conn, params) do
    id = Integer.parse(Map.get(params, "id")) |> elem(0)
    with {:ok, stock} <- StockService.get(conn.assigns.business_id, id) do
      conn
      |> put_view(StockView)
      |> render("show.json", stock: stock)
    end
  end

  def create(conn, new_material) do
    with :ok <- MaterialContract.validate(new_material),
        {:ok, %MaterialInput{} = input} <- MaterialInput.from_request_params(new_material),
        {:ok, %MaterialModel{} = inserted_material} <- MaterialService.create(conn.assigns.business_id, input) do
            conn
            |> put_status(:created)
            |> put_resp_header("location", material_path(conn, :show, inserted_material))
            |> render("show.json", material: inserted_material)
    end
  end

  def get_properties_list(conn, params) do
    with {:ok, properties} <- MaterialService.list_properties(conn.assigns.business_id) do
      json(conn, properties)
    end
  end

  def create_stock(conn, %{"id" => id, "stock" => new_stock}) do
    id = Integer.parse(id) |> elem(0)
    with :ok <- StockContract.validate(new_stock),
        {:ok, %StockInput{} = input} <- StockInput.from_request_params(new_stock),
        {:ok, _} <- MaterialService.assign_stock(conn.assigns.business_id, id, input) do
          conn
            |> send_resp(:created, "")
    end
  end

  def show(conn, %{"id" => id}) do
    with {:ok, material} <- MaterialService.get(conn.assigns.business_id, id) do
      render(conn, "show.json", material: material)
    end
  end

  def show_simplified(conn, %{"id" => id}) do
    with {:ok, material} <- Inventories.get_material_simplified(conn.assigns.business_id, id) do
      render(conn, "show_simplified.json", material: material)
    end
  end

  @spec update(atom | %{assigns: atom | %{business_id: any}}, map) :: any
  def update(conn, new_material) do
    id = Integer.parse(new_material |> Map.get("id")) |> elem(0)

    with :ok <- MaterialContract.validate(new_material, MaterialContract.get_update()),
        {:ok, %MaterialInput{} = input} <- MaterialInput.from_request_params(new_material),
         {:ok, %MaterialModel{} = material} <- MaterialService.update(conn.assigns.business_id, id, input) do
      render(conn, "show.json", material: material)
    end
  end

  @spec update_stock(atom | %{assigns: atom | %{business_id: any}}, map) :: any
  def update_stock(conn, %{"id" => id, "material_id" => material_id, "stock" => new_stock}) do
    id = Integer.parse(id) |> elem(0)
    material_id = Integer.parse(material_id) |> elem(0)
    with :ok <- StockContract.validate(new_stock),
        {:ok, %StockInput{} = input} <- StockInput.from_request_params(new_stock),
        {:ok, %{:updated_stock => updated_stock}} <- MaterialService.update_stock( conn.assigns.business_id, id, material_id, input) do
        conn
          |> put_view(StockView)
          |> render("show.json", stock: updated_stock)
    end
  end

  def delete(conn, %{"id" => id}) do
    with {:ok, %MaterialModel{}} <- MaterialService.delete(conn.assigns.business_id, id) do
      send_resp(conn, :no_content, "")
    end
  end

  def delete_stock(conn, %{"id" => id}) do
    id = Integer.parse(id) |> elem(0)

    with {:ok, _} <- MaterialService.delete_stock(conn.assigns.business_id, id) do
      send_resp(conn, :no_content, "")
    end
  end
end
