defmodule PiteraWeb.RegistrationController do
  use PiteraWeb, :controller

  alias Pitera.RegisterUser
  alias PiteraWeb.SessionController

  def new(conn, _params) do
    changeset = Pow.Plug.change_user(conn)
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    case RegisterUser.execute(user_params) do
      {:ok, _} ->
        SessionController.create(conn, %{"user" => user_params})
      {:error, some_error} ->
        render(conn, "new.html", changeset: some_error)
    end
  end
end
