defmodule PiteraWeb.Inputs.AddedCostInput do
  defstruct [
    :id,
    :description,
    :price
  ]
end
