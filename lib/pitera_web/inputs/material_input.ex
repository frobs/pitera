defmodule PiteraWeb.Inputs.MaterialInput do
  @moduledoc false
  require Logger
  alias PiteraWeb.Inputs.MaterialInput
  alias PiteraWeb.Inputs.StockInput
  alias Pitera.Helpers.StructHelper

  defstruct [
    :id,
    :business_id,
    :name,
    annotation: "",
    minimun_units: 0.0,
    tags: [],
    sku: "",
    stock: %StockInput{},
    properties: []
  ]

  def from_request_params(params) do
    try do
    {_ok, converted_stock} =
        params
        |> Map.get("stock", %{})
        |> StockInput.from_request_params()

      {:ok,
       %MaterialInput{
         id: params |> Map.get("id"),
         business_id: params |> Map.get("business_id"),
         name: params |> Map.get("name"),
         minimun_units: params |> Map.get("minimun_units", 0.0),
         annotation: params |> Map.get("annotation", ""),
         sku: params |> Map.get("sku", ""),
         tags: params |> Map.get("tags", []),
         stock: converted_stock,
         properties: params |> Map.get("properties", %{})
       }}
    rescue
      e in RuntimeError ->
        Logger.error("Parsing material input: #{e.message}")
        {:error, :internal_service_error}
    end
  end
end
