defmodule PiteraWeb.Inputs.StockInput do
  require Logger
  alias PiteraWeb.Inputs.StockInput
  alias PiteraWeb.Inputs.AddedCostInput
  alias Pitera.Helpers.DateHelper
  alias Pitera.Helpers.StructHelper

  defstruct [
    :id,
    :material_id,
    :business_id,
    :units,
    :unit_price,
    :acquisition_date,
    added_costs: [],
  ]

  def from_request_params(params) do
    try do
       converted_added_costs = Map.get(params, "added_costs", []) |> Enum.map(fn cost -> StructHelper.struct_from_map(cost, as: %AddedCostInput{}) end)
      {:ok, %StockInput {
        id: params |> Map.get("id"),
        units: params |> Map.get("units", 0.0),
        unit_price: params |> Map.get("unit_price", 0.0),
        acquisition_date: params |> Map.get("acquisition_date", DateHelper.get_current_date_as_string()),
        added_costs: converted_added_costs,
        material_id: params |> Map.get("material_id"),
        business_id: params |> Map.get("business_id")
      }}
    rescue
      e in RuntimeError ->
        Logger.error("Parsing stock input: #{e.message}")
        {:error, :internal_service_error}
    end
  end

  def from_material_params(params) do
    try do
      converted_added_costs = Map.get(params, "added_costs", []) |> Enum.map(fn cost -> StructHelper.struct_from_map(cost, as: %AddedCostInput{}) end)
      {:ok, %StockInput {
        id: params |> Map.get("id"),
        units: params |> Map.get("units", 0.0),
        unit_price: params |> Map.get("unit_price", 0.0),
        acquisition_date: params |> Map.get("acquisition_date", DateHelper.get_current_date_as_string()),
        added_costs: converted_added_costs,
        material_id: params |> Map.get("material_id"),
        business_id: params |> Map.get("business_id")
      }}
    rescue
      e in RuntimeError ->
        Logger.error("Parsing stock input: #{e.message}")
        {:error, :internal_service_error}
    end
  end
end
