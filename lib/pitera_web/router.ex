defmodule PiteraWeb.Router do
  use PiteraWeb, :router
  use Pow.Phoenix.Router
  use Pow.Extension.Phoenix.Router, otp_app: :pitera

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Pow.Plug.RequireNotAuthenticated,
      error_handler: PiteraWeb.AuthErrorHandler
  end

  pipeline :protected_browser do
    plug :accepts, ["html", "json"]
    plug :fetch_session
    plug :fetch_flash
    #plug :protect_from_forgery
    plug Pow.Plug.RequireAuthenticated,
      error_handler: PiteraWeb.AuthErrorHandler
    plug :put_secure_browser_headers
    plug PiteraWeb.Plugs.SetCurrentUser
  end

  scope "/" do
    pipe_through :browser
    pow_extension_routes()
  end

  scope "/" do
    pipe_through :protected_browser
    # Add public routes below
  end

  scope "/", PiteraWeb do
    pipe_through :browser
    get "/signup", RegistrationController, :new, as: :signup
    post "/signup", RegistrationController, :create, as: :signup
    get "/login", SessionController, :new, as: :login
    post "/login", SessionController, :create, as: :login
    # Add public routes below
  end

  scope "/", PiteraWeb do
    pipe_through :protected_browser
    # Add protected routes below
    delete "/logout", SessionController, :delete, as: :logout

    resources "/materials", MaterialController, except: [:new, :edit]
    get "/materials/:id/simplified", MaterialController, :show_simplified
    get "/materials_properties", MaterialController, :get_properties_list
    get "/materials/:id/stocks", MaterialController, :get_stock_list
    get "/materials/stocks/:id", MaterialController, :get_stock_by_id
    post "/materials/:id/stocks/new", MaterialController, :create_stock
    put "/materials/:material_id/stocks/:id", MaterialController, :update_stock
    delete "/materials/stocks/:id", MaterialController, :delete_stock
    get "/*catch_all", PageController, :index
  end
end
