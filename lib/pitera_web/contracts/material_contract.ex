defmodule PiteraWeb.Contracts.MaterialContract do
  @moduledoc false
  alias PiteraWeb.Contracts.Contract
  alias PiteraWeb.Contracts.StockContract
  use Contract
  @behaviour Contract

  def get() do
    %{
      "type" => "object",
      "required" => ["name", "stock"],
      "properties" => %{
        "name" => %{"type" => "string", "minLength" => 1, "maxLength" => 200},
        "annotation" => %{"type" => "string", "maxLength" => 400},
        "sku" => %{"type" => "string", "maxLength" => 200},
        "stock" => StockContract.get(),
        "minimun_units" => %{"type" => "number"},
        "properties" => %{"type" => "object"}
      }
    }
  end

  def get_update() do
    %{
      "type" => "object",
      "required" => ["name"],
      "properties" => %{
        "name" => %{"type" => "string", "minLength" => 1, "maxLength" => 200},
        "annotation" => %{"type" => "string", "maxLength" => 400},
        "sku" => %{"type" => "string", "maxLength" => 200},
        "minimun_units" => %{"type" => "number"},
        "properties" => %{"type" => "object"}
      }
    }
  end
end
