defmodule PiteraWeb.Contracts.ValidationError do
  @moduledoc false
  defstruct [:field, :description]
end
