defmodule PiteraWeb.Contracts.Contract do
  alias PiteraWeb.Contracts.ValidationError

  @moduledoc false
  @callback get()::%{}

  defmacro __using__(_) do
    quote([]) do
      def validate(map_to_be_validated) do
        case ExJsonSchema.Validator.validate(get() |> ExJsonSchema.Schema.resolve, map_to_be_validated) do
          :ok -> :ok
          {:error, validation_errors} -> {:input_contract_invalid, validation_errors |> Enum.map(&translate_to_error/1)}
        end
      end

      def validate(map_to_be_validated, schema) do
        case ExJsonSchema.Validator.validate(schema |> ExJsonSchema.Schema.resolve, map_to_be_validated) do
          :ok -> :ok
          {:error, validation_errors} -> {:input_contract_invalid, validation_errors |> Enum.map(&translate_to_error/1)}
        end
      end

      defp translate_to_error(validation_error) do
        %ValidationError{field: elem(validation_error,1), description: elem(validation_error,0)}
      end
      defoverridable [validate: 1]
    end
  end

  @callback validate(%{}) :: atom | {atom, list()}
end
