defmodule PiteraWeb.Contracts.StockContract do
  @moduledoc false
  alias PiteraWeb.Contracts.Contract
  use Contract
  @behaviour Contract

  def get() do
    %{
      "type" => "object",
      "required" => ["units", "unit_price"],
      "properties" => %{
        "unit_price" => %{
          "type" => "number"
        },
        "acquisition_date" => %{
          "type" => "string",
          "format" => "date"
        },
        "units" => %{
          "type" => "number"
        },
        "added_costs" => %{
          "type" => "array",
          "items" => %{
            "type" => "object",
            "required" => ["description", "price"],
            "properties" => %{
              "id" => %{
                "type" => "number"
              },
              "description" => %{
                "type" => "string"
              },
              "price" => %{
                "type" => "number"
              }
            }
          }
        }
      }
    }
  end


end
