defmodule PiteraWeb.AddedCostView do
  use PiteraWeb, :view
  alias PiteraWeb.AddedCostView

  def render("index.json", %{added_cost: added_cost}) do
    %{data: render_many(added_cost, AddedCostView, "added_cost.json")}
  end

  def render("show.json", %{added_cost: added_cost}) do
    %{data: render_one(added_cost, AddedCostView, "added_cost.json")}
  end

  def render("added_cost.json", %{added_cost: added_cost}) do
    %{
      description: added_cost.concept,
      price: added_cost.total
    }
  end

end
