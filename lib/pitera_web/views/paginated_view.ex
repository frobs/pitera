defmodule PiteraWeb.PaginatedView do
  use PiteraWeb, :view

  def render("index.json", %{paginated_results: paginated_results, module: module, template: template}) do
    %{
      data: render_many(paginated_results.entries, module, template),
      pagination: %{
        page_number: paginated_results.page_number,
        page_size: paginated_results.page_size,
        total_pages: paginated_results.total_pages,
        total_results: paginated_results.total_results
      }
    }
  end

end
