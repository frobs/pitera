defmodule PiteraWeb.StockView do
  use PiteraWeb, :view
  alias PiteraWeb.StockView
  alias PiteraWeb.AddedCostView

  def render("index.json", %{stock: stock}) do
    %{data: render_many(stock, StockView, "stock.json")}
  end

  def render("show.json", %{stock: stock}) do
    %{data: render_one(stock, StockView, "stock.json")}
  end

  def render("stock_table.json", %{stocks: stocks}) do
    %{
      data: render_many(stocks, StockView, "stock.json"),
      count: Enum.count(stocks)
    }
  end

  def render("stock.json", %{stock: stock}) do
    %{
      id: stock.id,
      units: stock.units,
      unit_price: stock.unit_price,
      total_unit_price: stock.total_unit_price,
      total_price: stock.total_price,
      total_added_cost: stock.total_added_cost,
      acquisition_date: stock.acquisition_date,
      updated_at: stock.updated_at,
      added_costs: render_many(stock.added_costs, AddedCostView, "added_cost.json")
    }
  end

end
