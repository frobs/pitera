defmodule PiteraWeb.MaterialView do
  use PiteraWeb, :view
  alias PiteraWeb.MaterialView

  def render("show.json", %{material: material}) do
    %{data: render_one(material, MaterialView, "material.json")}
  end

  def render("show_simplified.json", %{material: material}) do
    %{data: render_one(material, MaterialView, "material_simplified.json")}
  end

  def render("material.json", %{material: material}) do
    %{
      id: material.id,
      name: material.name,
      annotation: material.annotation,
      sku: material.sku,
      annotation: material.annotation,
      average_unit_price: material.average_unit_price,
      minimun_units: material.minimun_units,
      total_units: material.total_units,
      total_price: material.total_price,
      updated_at: material.updated_at,
      properties: material.properties
    }
  end

  def render("material_simplified.json", %{material: material}) do
    %{
      id: material.id,
      name: material.name,
      sku: material.sku
    }
  end

end
