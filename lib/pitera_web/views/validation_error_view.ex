defmodule PiteraWeb.ValidationErrorView do
  use PiteraWeb, :view
  alias PiteraWeb.ValidationErrorView

  def render("index.json", %{validation_error: validation_error}) do
    %{errors: render_many(validation_error, ValidationErrorView, "validation_error.json")}
  end

  def render("show.json", %{validation_error: validation_error}) do
    %{errors: render_one(validation_error, ValidationErrorView, "validation_error.json")}
  end

  def render("validation_error.json", %{validation_error: validation_error}) do
    %{
      description: validation_error.description,
      field: validation_error.field
    }
  end

end
