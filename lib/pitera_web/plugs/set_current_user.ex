defmodule PiteraWeb.Plugs.SetCurrentUser do
  import Plug.Conn

  def init(_params) do
  end

  def call(conn, _params) do
    current_user = Pow.Plug.current_user(conn)

    cond do
      current_user != nil ->
        conn
        |> assign(:business_id, current_user|> Map.get(:default_account_id))
        |> assign(:user, current_user)
      true ->
        conn
        |> assign(:business_id, nil)
        |> assign(:user, nil)
    end
  end
end
