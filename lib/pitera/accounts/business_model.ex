defmodule Pitera.Accounts.BusinessModel do
  use Ecto.Schema
  import Ecto.Changeset
  alias Pitera.Accounts.AccountModel

  schema "business" do
    field :cif, :string
    field :name, :string
    field :email, :string
    field :phone, :string
    field :web, :string
    field :phone_2, :string
    field :address, :string
    field :postal_code, :string
    field :town, :string
    field :region, :string
    field :country, :string
    has_many :accounts, AccountModel, foreign_key: :business_id
    timestamps(type: :utc_datetime)
  end

  @doc false
  @required_fields ~w(cif name)a
  @optional_fields ~w( cif phone phone_2 web fax address postal_code town region country )a

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:email, message: "This email is already used")
  end
end
