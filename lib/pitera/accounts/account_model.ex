defmodule Pitera.Accounts.AccountModel do
  use Ecto.Schema
  import Ecto.Changeset
  alias Pitera.Accounts.BusinessModel
  alias Pitera.Users.User

  schema "accounts" do
    field :permissions, :integer
    belongs_to :users, User, foreign_key: :user_id
    belongs_to :business, BusinessModel, foreign_key: :business_id
  end

  @required_fields ~w(permissions business_id)a
  @optional_fields ~w(user_id)a

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields + @optional_fields)
    |> validate_required(@required_fields)
    |> unique_constraint(:user_company_constraint, name: :user_company_index, message: "This user already have an account for this company")
  end

end
