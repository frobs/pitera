defmodule Pitera.Users.User do
  import Ecto.Changeset
  use Ecto.Schema
  use Pow.Ecto.Schema,
  password_hash_methods: {&Argon2.hash_pwd_salt/1,
                            &Argon2.verify_pass/2}
  use Pow.Extension.Ecto.Schema,
    extensions: [PowResetPassword]
  alias Pitera.Accounts.AccountModel

  schema "users" do
    pow_user_fields()
    field :name, :string
    belongs_to :default_account, AccountModel, foreign_key: :default_account_id
    has_many :accounts, AccountModel, foreign_key: :user_id

    timestamps()
  end

  def changeset(user_or_changeset, attrs \\ %{}) do
    user_or_changeset
    |> cast(attrs, [:name])
    |> pow_changeset(attrs)
    |> pow_extension_changeset(attrs)
  end

end
