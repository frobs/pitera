defmodule Pitera.RegisterUser do
  import Ecto.Query, warn: false
  alias Pitera.Repo
  alias Ecto.Multi
  alias Pitera.Accounts.BusinessModel
  alias Pitera.Accounts.AccountModel
  alias Pitera.Users.User
  @behaviour Pitera.Flow

  def execute(new_user) do
    Multi.new()
    |> Multi.insert(:created_business, %BusinessModel{
      name: new_user |> Map.get("business_name"),
      cif: new_user |> Map.get("cif"),
      email:  new_user |> Map.get("email")
    })
    |> Multi.insert(:created_user, User.changeset(%User{}, %{
      name:  new_user |> Map.get("name"),
      email:  new_user |> Map.get("email"),
      password:  new_user |> Map.get("password"),
      confirm_password:  new_user |> Map.get("confirm_password")
    }))
    |> Multi.insert(:account, fn %{created_business: created_business, created_user: created_user} ->
      %AccountModel{
        permissions: 1,
        users: created_user,
        business: created_business
    }
    end)
    |> Multi.update(:new_user, fn %{created_business: created_business, created_user: created_user, account: account} ->
      created_user
        |> Repo.preload(:accounts)
        |> Repo.preload(:default_account)
        |> Ecto.Changeset.change
        |> Ecto.Changeset.put_assoc(:accounts, [account])
        |> Ecto.Changeset.put_assoc(:default_account, account)
    end)
    |> Repo.transaction()
  end
end
