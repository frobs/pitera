defmodule Pitera.Flow do
  @callback execute(state :: term) :: {:ok, new_state :: term} | {:error, reason :: term}
end
