defmodule Pitera.Inventories.ProductModel do
  use Ecto.Schema
  import Ecto.Changeset
  alias Pitera.Inventories.StockModel
  alias Pitera.Accounts.BusinessModel
  alias Pitera.Inventories.MaterialModel

  schema "products" do
    field :name, :string
    field :sku, :string
    field :annotation, :string
    field :total_units, :float
    field :total_cost, :float
    field :average_unit_cost, :float
    field :profit_percentage, :float
    field :discount, :float
    field :pvp, :float
    field :minimun_units, :float
    field :type, :integer
    field :manufacturing_cost, :float
    field :properties, :map
    belongs_to :business, BusinessModel
    has_many :stock, StockModel, foreign_key: :material_id, on_delete: :delete_all
    many_to_many( :materials, MaterialModel, join_through: "products_materials", join_keys: [product_id: :id, material_id: :id], on_replace: :delete )
    timestamps(type: :utc_datetime)
  end

  @required_fields ~w(name business_id average_unit_cost total_units total_cost pvp type)a
  @optional_fields ~w(sku annotation profit_percentage discount minimun_units manufacturing_cost)a

  @doc false
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields ++ @optional_fields)
    |> cast_embed(:properties)
    |> validate_required(@required_fields)
    |> foreign_key_constraint(:business_id)
    |> unique_constraint(:business_sku_constraint, name: :business_material_sku_index, message: "A material with same sku already exist for this business")
  end
end
