defmodule Pitera.Inventories.Inventories do
  @moduledoc """
  The Inventories context.
  """
  import Ecto.Query, warn: false
  alias Pitera.Repo
  alias Ecto.Multi
  alias Pitera.Inventories.MaterialModel
  alias Pitera.Inventories.StockModel
  alias Pitera.Tags.TagModel
  alias Pitera.Helpers.NumberHelper
  alias Pitera.Helpers.StructHelper
  require Logger


  defp build_list_material_query(query, params) do
    Enum.reduce(params, query, &compose_list_material_query/2)
  end

  defp compose_list_material_query({"search", search}, query) do
    where(query, [material], ilike(material.name, ^"%#{search}%") or ilike(material.sku, ^"%#{search}%") or ilike(material.annotation, ^"%#{search}%"))
  end

  defp compose_list_material_query({"just_minimun_units", just_minimun_units}, query) do
    if(just_minimun_units === "true") do
      where(query, [material], material.total_units <= material.minimun_units)
    else 
      query
    end
  end

  defp compose_list_material_query({"properties", properties}, query) do
    where(query, [material], fragment(
      "(properties)::jsonb @> ?::jsonb",^properties
    ))
  end

  defp compose_list_material_query(_unsupported_param, query) do
    query
  end

  def list_material(business_id, params) do
    material_list = from(material in MaterialModel) 
    |> build_list_material_query(params)
    |> Persistence.Query.list_paginated_ordered(business_id, params)
    
    try do
    {:ok, material_list }
    rescue
      e in RuntimeError ->
        Logger.error("Error retrieving list of materials #{e.message}")
        {:error, :persistence_error}
    end
  end

  def list_material_properties(business_id) do
    try do
      material_properties = Repo.all(
        from(material in MaterialModel,
          where: material.business_id == ^business_id,
          select: material.properties
        ))

      grouped_properties = material_properties
      |> Enum.flat_map(fn material_property -> material_property end)
      |> Enum.group_by(fn {key, value} -> key end, fn {key,value} -> value end) |> Enum.sort_by(fn {key,value} -> key end, :asc)
      |> Enum.into(%{})

        {:ok, grouped_properties}
    rescue
      e in RuntimeError ->
        Logger.error("Error retrieving list of materials #{e.message}")
        {:error, :persistence_error}
    end
  end

  def get_material(business_id, id) do
    try do
    {:ok, Repo.one!(
        from(material in MaterialModel,
          where: material.id == ^id,
          where: material.business_id == ^business_id
        )
      )}
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      e in RuntimeError ->
        Logger.error("Error retrieving a material #{e.message}")
        {:error, :persistence_error}
    end
  end

  def get_material_simplified(business_id, id) do
    try do
      {:ok, Repo.one!(
        from(material in MaterialModel,
          where: material.id == ^id,
          where: material.business_id == ^business_id,
          select: %{id: material.id, name: material.name, sku: material.sku}
        )
      )}
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      e in RuntimeError ->
        Logger.error("Error retrieving a simplified material #{e.message}")
        {:error, :persistence_error}
    end
  end

  def list_stock_by_material(business_id, id) do
    try do
      {:ok, Repo.all(
          from(stock in StockModel,
            where: stock.material_id == ^id,
            where: stock.business_id == ^business_id
          )
        )
      }
    rescue
      e in RuntimeError ->
        Logger.error("Error retrieving material stock #{e.message}")
        {:error, :persistence_error}
    end
  end

  def get_stock_by_id(business_id, id) do
    try do
      {:ok, Repo.one!(
        from(stock in StockModel,
          where: stock.id == ^id,
          where: stock.business_id == ^business_id
        )
      )}
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      e in RuntimeError ->
        Logger.error("Error retrieving a stock by id #{e.message}")
        {:error, :persistence_error}
    end
  end

  def create_material(%MaterialModel{} = new_material) do
    try do
      new_material
        |> MaterialModel.changeset()
        |> Repo.insert!()
        |> Repo.preload(:tags)
        |> Ecto.Changeset.change
        |> Ecto.Changeset.put_assoc(:tags, material_tags(new_material))
        |> Repo.update()
    rescue
      e in RuntimeError ->
        Logger.error("Error creating material #{e.message}")
        {:error, :persistence_error}
    end
  end

  defp material_tags(attrs) do
    tags = attrs.tags
    Repo.insert_all(TagModel, tags, on_conflict: :nothing)
    tag_names = for t <- tags, do: t.normalized_name
    Repo.all(from t in TagModel, where: t.normalized_name in ^tag_names)
  end

  def assign_stock_to_material(%StockModel{} = new_stock) do
    try do
      with {:ok, %MaterialModel{} = material} <- get_material(new_stock.material_id, new_stock.business_id) do
        Multi.new()
        |> Multi.insert(:inserted_stock, new_stock)
        |> Multi.update(:updated_material, fn %{inserted_stock: inserted_stock} ->
          {:ok, material_stock} = list_stock_by_material(inserted_stock.business_id, inserted_stock.material_id)
          {total_units, total_price} = calculate_total_units_and_price(material_stock)

          material
          |> Ecto.Changeset.change(%{
            total_units: total_units,
            total_price: total_price,
            average_unit_price: NumberHelper.divide(total_price, total_units)
          })
        end)
        |> Repo.transaction()
      end
    rescue
      e in RuntimeError ->
        Logger.error("Error creating material #{e.message}")
        {:error, :persistence_error}
    end
  end

  def update_material( %MaterialModel{} = new_material) do
    try do
      with {:ok, %MaterialModel{} = material} <- get_material(new_material.business_id, new_material.id) do
        MaterialModel.changeset(material, StructHelper.map_from_struct(new_material))
          |> Repo.update()
      end
    rescue
      e in RuntimeError ->
        Logger.error("Error creating material #{e.message}")
        {:error, :persistence_error}
    end
  end

  def update_stock( %StockModel{} = new_stock) do
    try do
      with {:ok, %StockModel{} = old_stock} <- get_stock_with_material(new_stock.business_id, new_stock.id) do
        Multi.new()
        |> Multi.update(:updated_stock, StockModel.changeset(old_stock, StructHelper.map_from_struct(new_stock)))
        |> Multi.update(:updated_material, fn %{updated_stock: updated_stock} ->
          {:ok, material_stock} = list_stock_by_material(old_stock.business_id, old_stock.materials.id)

          {total_units, total_price} = calculate_total_units_and_price(material_stock)
          material = old_stock.materials

          material
          |> Ecto.Changeset.change(%{
            total_units: if(total_units >= 0) do total_units else 0.0 end,
            total_price: if(total_price >= 0) do total_price else 0.0 end,
            average_unit_price: NumberHelper.divide(total_price, total_units)
          })
        end)
        |> Repo.transaction()
      end
    rescue
      e in RuntimeError ->
        Logger.error("Error updating material stock #{e.message}")
        {:error, :persistence_error}
    end
  end

  defp calculate_total_units_and_price(stock_list) do
    Enum.reduce(stock_list, {0.0,0.0}, fn stock, acc -> {elem(acc,0) + stock.units, elem(acc,1) + stock.total_price} end)
  end

  defp get_stock_with_material(business_id, id) do
    try do
      {:ok,  Repo.one!(
        from(stock in StockModel,
          where: stock.id == ^id,
          where: stock.business_id == ^business_id,
          left_join: materials in assoc(stock, :materials),
          left_join: business in assoc(stock, :business),
          preload: [materials: materials, business: business]
        )
      )}
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      e in RuntimeError ->
        Logger.error("Error retrieving a material #{e.message}")
        {:error, :persistence_error}
    end
  end

  def delete_material_stock(business_id, id) do
    try do
      with {:ok, %StockModel{} = stock} <-  get_stock_with_material(business_id, id) do
        material = stock.materials

        Multi.new()
        |> Multi.delete(:deleted_stock, stock)
        |> Multi.update(:updated_material, fn %{deleted_stock: deleted_stock} ->
          {:ok, material_stock} = list_stock_by_material(deleted_stock.business_id, deleted_stock.materials.id)
          {total_units, total_price} = calculate_total_units_and_price(material_stock)

          material
          |> Ecto.Changeset.change(%{
            total_units: total_units,
            total_price: total_price,
            average_unit_price: NumberHelper.divide(total_price, total_units)
          })
        end)
        |> Repo.transaction()
      end
    rescue
      e in RuntimeError ->
        Logger.error("Error updating material stock #{e.message}")
        {:error, :persistence_error}
    end
  end

  def delete_material(business_id, id) do
    try do
      with {:ok, %MaterialModel{} = material} <- get_material(business_id, id) do
        material |> Repo.delete()
        end
    rescue
      e in RuntimeError ->
        Logger.error("Error updating material stock #{e.message}")
        {:error, :persistence_error}
    end
  end

end
