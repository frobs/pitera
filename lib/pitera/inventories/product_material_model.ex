defmodule ProductMaterialModel do
  use Ecto.Schema
  import Ecto.Changeset

  alias Pitera.Inventories.MaterialModel
  alias Pitera.Inventories.ProductModel

  @primary_key false
  schema "products_materials" do
    belongs_to(:products, ProductModel, primary_key: true)
    belongs_to(:materials, MaterialModel, primary_key: true)
  end

  @required_fields ~w(product_id material_id)a
  def changeset(user_project, params \\ %{}) do
    user_project
    |> cast(params, @required_fields)
    |> validate_required(@required_fields)
    |> foreign_key_constraint(:product_id)
    |> foreign_key_constraint(:material_id)
    |> unique_constraint([:products, :materials],
      name: :product_id_material_id_unique_index,
      message: "This product and material are already related"
    )
  end
end
