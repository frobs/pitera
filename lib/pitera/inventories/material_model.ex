defmodule Pitera.Inventories.MaterialModel do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset
  alias Pitera.Inventories.StockModel
  alias Pitera.Tags.TagModel
  alias Pitera.Accounts.BusinessModel
  alias Pitera.Inventories.ProductModel
  alias Pitera.Repo

  schema "materials" do
    field :name, :string
    field :sku, :string
    field :annotation, :string
    field :average_unit_price, :float
    field :minimun_units, :float
    field :total_units, :float
    field :total_price, :float
    field :properties, :map
    belongs_to :business, BusinessModel
    has_many :stock, StockModel, foreign_key: :material_id, on_delete: :delete_all
    many_to_many(
      :tags,
      TagModel,
      join_through: "tags_relations",
      join_keys: [
        material_id: :id,
        tag_id: :id
      ],
      on_replace: :delete
    )
    many_to_many(
      :products,
      ProductModel,
      join_through: "products_materials",
      join_keys: [
        material_id: :id,
        product_id: :id
      ],
      on_replace: :delete
    )
    timestamps(type: :utc_datetime)
  end

  @required_fields ~w(name business_id average_unit_price total_units total_price)a
  @optional_fields ~w(sku annotation minimun_units properties)a

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> foreign_key_constraint(:business_id)
    |> unique_constraint(
         :business_sku_constraint,
         name: :business_material_sku_index,
         message: "A material with same sku already exist for this business"
       )
  end


end
