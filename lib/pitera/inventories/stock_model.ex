defmodule Pitera.Inventories.StockModel do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset
  alias Pitera.Inventories.MaterialModel
  alias Pitera.Accounts.BusinessModel
  alias Pitera.Inventories.AddedCostModel

  schema "stock" do
    field :units, :float
    field :unit_price, :float
    field :total_unit_price, :float
    field :total_added_cost, :float
    field :total_price, :float
    field :acquisition_date, :date
    belongs_to :business, BusinessModel
    belongs_to :materials, MaterialModel, foreign_key: :material_id
    embeds_many :added_costs, AddedCostModel, on_replace: :delete
    timestamps(type: :utc_datetime)
  end

  @required_fields ~w(units business_id unit_price total_unit_price total_added_cost total_price material_id acquisition_date)a
  @optional_fields ~w()a

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields ++ @optional_fields)
    |> cast_embed(:added_costs)
    |> validate_required(@required_fields)
    |> foreign_key_constraint(:material_id)
  end
end
