defmodule Pitera.Inventories.AddedCostModel do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false
  embedded_schema do
    field :concept, :string
    field :total, :float
  end

  @required_fields ~w(concept total)a

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields)
    |> validate_required(@required_fields)
  end
end
