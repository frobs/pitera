defmodule Pitera.Inventories.MaterialService do
  @moduledoc false
  alias Pitera.Inventories.Inventories
  alias Pitera.Inventories.StockService
  alias Pitera.Tags.TagService
  alias Pitera.Inventories.MaterialModel
  alias PiteraWeb.Inputs.MaterialInput
  alias PiteraWeb.Inputs.StockInput
  alias Pitera.Helpers.NumberHelper
  alias Pitera.Tags.TagModel
  require Logger

  def create(business_id, %MaterialInput{} = new_material) do
    try do
      input_to_model(new_material, business_id)
        |> Inventories.create_material()
    rescue
     e in RuntimeError ->
       Logger.error("Error creating material #{e.message}")
       {:error, :internal_service_error}
    end
  end

  def assign_stock(business_id, material_id, %StockInput{} = new_stock) do
    try do
      StockService.input_to_model(new_stock, business_id, material_id)
      |> Inventories.assign_stock_to_material()
    rescue
      e in RuntimeError ->
        Logger.error("Error when assign stock to material #{e.message}")
        {:error, :internal_service_error}
    end
  end

  def update(business_id, material_id, %MaterialInput{} = new_material) do
    try do
      input_to_model(new_material, business_id, material_id)
      |> Inventories.update_material()
    rescue
      e in RuntimeError ->
        Logger.error("Error updating material #{e.message}")
        {:error, :internal_service_error}
    end
  end

  def update_stock(business_id, stock_id, material_id, %StockInput{} = new_stock) do
    try do
      StockService.input_to_model(new_stock, business_id, material_id)
      |> Map.update(:id, stock_id, &(&1))
      |> Inventories.update_stock()
    rescue
      e in RuntimeError ->
        Logger.error("Error creating stock #{e.message}")
        {:error, :internal_service_error}
    end
  end

  def list(business_id, params) do
    try do
      Inventories.list_material(business_id, params)
    rescue
      e in RuntimeError ->
        Logger.error("Error retrieving list of materials #{e.message}")
        {:error, :internal_service_error}
    end
  end

  def list_properties(business_id) do
    try do
      Inventories.list_material_properties(business_id)
    rescue
      e in RuntimeError ->
        Logger.error("Error retrieving list of materials properties #{e.message}")
        {:error, :internal_service_error}
    end
  end

  def list_stock(business_id, id) do
    try do
      Inventories.list_stock_by_material(business_id, id)
    rescue
      e in RuntimeError ->
        Logger.error("Error retrieving a material simplified #{e.message}")
        {:error, :internal_service_error}
    end
  end

  def get(business_id, id) do
    try do
      Inventories.get_material(business_id, id)
    rescue
      e in RuntimeError ->
        Logger.error("Error retrieving a material #{e.message}")
        {:error, :internal_service_error}
    end
  end

  def get_simplified(business_id, id) do
    try do
      Inventories.get_material_simplified(business_id, id)
    rescue
      e in Ecto.NoResultsError ->
        Logger.error("Error retrieving material simplified #{e.message}")
        {:error, :not_found}
      e in RuntimeError ->
        Logger.error("Error retrieving a material simplified #{e.message}")
        {:error, :internal_service_error}
    end
  end

  def delete(business_id, id) do
    try do
      Inventories.delete_material(business_id, id)
    rescue
      e in RuntimeError ->
        Logger.error("Error retrieving a material #{e.message}")
        {:error, :internal_service_error}
    end
  end

  def delete_stock(business_id, id) do
    try do
      Inventories.delete_material_stock(business_id, id)
    rescue
      e in RuntimeError ->
        Logger.error("Error retrieving a material #{e.message}")
        {:error, :internal_service_error}
    end
  end

  def input_to_model(%MaterialInput{} = material, business_id \\ nil, material_id \\ nil ) do
    id = if (material_id != nil), do: material_id, else: material.id
    business = if (business_id != nil), do: business_id, else: material.business_id
    processed_stock = StockService.input_to_model(material.stock, business, id)
    units = NumberHelper.round_to_four_decimals(material.stock.units || 1)

    %MaterialModel {
      id: id,
      business_id: business,
      name: material.name,
      sku: material.sku,
      tags: Enum.map(material.tags, fn tag -> TagService.input_to_model(tag, business_id) end),
      annotation: material.annotation,
      minimun_units: NumberHelper.round_to_four_decimals(material.minimun_units),
      total_units: units,
      average_unit_price: NumberHelper.divide(processed_stock.total_price, units),
      total_price: processed_stock.total_price,
      properties: material.properties,
      stock: [processed_stock]
    }
  end

end
