defmodule Pitera.Inventories.StockService do
  @moduledoc false
  alias Pitera.Inventories.StockModel
  alias Pitera.Inventories.AddedCostModel
  alias Pitera.Inventories.Inventories
  alias PiteraWeb.Inputs.StockInput
  alias Pitera.Helpers.DateHelper
  alias Pitera.Helpers.NumberHelper
  require Logger

  def input_to_model(%StockInput{} = stock, business_id \\ nil, material_id \\ nil) do
    units = NumberHelper.round_to_four_decimals(stock.units || 1)
    unit_price = NumberHelper.round_to_four_decimals(stock.unit_price || 0.0)
    converted_added_costs = convert_added_costs(stock.added_costs || [])
    calculated_total_added_cost = calculate_added_cost_total(converted_added_costs)
    calculated_total_price = NumberHelper.multiply(units, unit_price) + calculated_total_added_cost
    calculated_total_unit_price = NumberHelper.divide(calculated_total_price, units)
    %StockModel{
      id: stock.id,
      business_id: business_id,
      material_id: material_id,
      units: units,
      unit_price: unit_price,
      total_unit_price: calculated_total_unit_price,
      total_price: calculated_total_price,
      total_added_cost: calculated_total_added_cost,
      acquisition_date: DateHelper.string_to_date(stock.acquisition_date) ,
      added_costs: converted_added_costs
    }
  end

  def get(business_id, id) do
    try do
      Inventories.get_stock_by_id(business_id, id)
    rescue
      e in RuntimeError ->
        Logger.error("Error retrieving a material simplified #{e.message}")
        {:error, :internal_service_error}
    end
  end

  defp convert_added_costs(input_added_costs) do
    input_added_costs
    |> Enum.map(fn added_cost ->
      %AddedCostModel{
        concept: added_cost.description,
        total: NumberHelper.round_to_four_decimals(added_cost.price)
      }
    end)
  end

  defp calculate_added_cost_total(added_costs) do
    Enum.reduce(added_costs, 0.0, fn cost, acc ->
      acc + NumberHelper.round_to_four_decimals(cost.total)
    end)
  end
end
