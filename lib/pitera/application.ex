defmodule Pitera.Application do
  use Application

  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec

    # Define workers and child supervisors to be supervised
    children = [
      #{Redix, {"redis://:secret@redix.example.com:6380/1", [name: :redix]}}
      {Redix, name: :redix},
      # Start the Ecto repository
      supervisor(Pitera.Repo, []),
      # Start the endpoint when the application starts
      supervisor(PiteraWeb.Endpoint, []),
      # Start your own worker by calling: Pitera.Worker.start_link(arg1, arg2, arg3)
      # worker(Pitera.Worker, [arg1, arg2, arg3]),
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Pitera.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    PiteraWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
