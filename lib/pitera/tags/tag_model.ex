defmodule Pitera.Tags.TagModel do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset
  alias Pitera.Accounts.BusinessModel
  alias Pitera.Inventories.MaterialModel

  schema "tags" do
    field :name, :string
    field :normalized_name, :string
    belongs_to :business, BusinessModel
    many_to_many( :materials, MaterialModel, join_through: "tags_relations", join_keys: [tag_id: :id, material_id: :id], on_replace: :delete )
  end

  @required_fields ~w(name normalized_name business_id)a
  @optional_fields ~w()a

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> unique_constraint(:business_normalized_tag_name_constraint,
         name: :business_normalized_tag_name_index
       )

  end
end
