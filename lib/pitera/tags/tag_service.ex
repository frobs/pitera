defmodule Pitera.Tags.TagService do
  @moduledoc false
  alias Pitera.Tags.TagModel
  alias Pitera.Helpers.StringHelper

  def input_to_model(tag_string, business_id \\ nil) do
    %{
      name: tag_string,
      normalized_name: StringHelper.normalize(tag_string),
      business_id: business_id
    }
  end

end
