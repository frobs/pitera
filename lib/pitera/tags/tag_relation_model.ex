defmodule Pitera.Tags.TagRelationModel do
  use Ecto.Schema
  import Ecto.Changeset

  alias Pitera.Inventories.MaterialModel
  alias Pitera.Tags.TagModel

  @primary_key false
  schema "tags_relations" do
    belongs_to(:tags, TagModel, primary_key: true)
    belongs_to(:materials, MaterialModel, primary_key: true)
  end

  @required_fields ~w(tag_id )a
  @optional_fields ~w(material_id)a
  def changeset(tags_relations, params \\ %{}) do
    tags_relations
    |> cast(params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> foreign_key_constraint(:tag_id)
    |> foreign_key_constraint(:material_id)
    |> unique_constraint(:tag_id_material_id_unique_constraint,
      name: :tag_id_material_id_unique_index
    )
  end
end
