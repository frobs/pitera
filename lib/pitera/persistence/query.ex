defmodule Persistence.Query do
  alias Persistence.Paginator
  import Ecto.Query

  def list(query, business_id) do
    query
    |> where([p], p.business_id == ^business_id)
  end

  def list_paginated(query, business_id, params) do
    query
    |> list(business_id)
    |> Paginator.new(params)
  end

  def list_paginated_ordered(query, business_id, params) do
    fields = params |> Map.get("sort_by", ["updated_at"]) |> Enum.map(fn x -> String.to_atom(x) end)
    order = process_descending(params |> Map.get("sort_desc", ["true"]))
    order_filter = Keyword.new(Enum.zip(order, fields) |> Enum.into(%{}))

    query
    |> order_by(^order_filter)
    |> list_paginated(business_id, params)
  end

  defp process_descending(order_list) do
    Enum.map(order_list, fn descending ->
      if(descending == "true") do
        :desc
      else
        :asc
      end
    end)
  end

end
