defmodule Persistence.Paginator do
  import Ecto.Query
  alias Persistence.Paginator
  alias Pitera.Repo

  defstruct [:entries, :page_number, :page_size, :total_results, :total_pages]

  def new(query, params) do
    page_number = params |> Map.get("page", 1) |> to_int
    page_size = params |> Map.get("page_size", 10) |> to_int
    total_results = total_results(query)

    %Paginator{
      entries: entries(query, page_number, page_size),
      page_number: page_number,
      page_size: page_size,
      total_results: total_results,
      total_pages: total_pages(total_results, page_size)
    }
  end

  defp ceiling(float) do
    t = trunc(float)

    case float - t do
      neg when neg < 0 ->
        t
      pos when pos > 0 ->
        t + 1
      _ -> t
    end
  end

  defp entries(query, page_number, page_size) do

    if(page_size == -1) do
      query
      |> Repo.all
    else
      offset = page_size * (page_number - 1)
      query
        |> limit([_], ^page_size)
        |> offset([_], ^offset)
        |> Repo.all
    end
  end

  defp to_int(i) when is_integer(i), do: i
  defp to_int(s) when is_binary(s) do
    case Integer.parse(s) do
      {i, _} -> i
      :error -> :error
    end
  end

  defp total_results(query) do
    query
    |> exclude(:order_by)
    |> exclude(:select)
    |> select([e], count(e.id))
    |> Repo.one
  end

  defp total_pages(_count, -1), do: 1

  defp total_pages(count, page_size) do
    ceiling(count / page_size)
  end
end
