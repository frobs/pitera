defmodule Pitera.Helpers.StringHelper do
  @moduledoc false
  def normalize(text) do
    text |> String.normalize(:nfd) |> String.trim() |> String.replace(~r/ +/, " ") |> String.replace(~r/[^A-z | 0-9\s]/u, "") |> String.replace(~r/\s/, "_") |> String.downcase()
  end
end
