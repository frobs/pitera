defmodule Pitera.Helpers.StructHelper do
  def struct_from_map(a_map, as: a_struct) do
    # Find the keys within the map
    keys =
      Map.keys(a_struct)
      |> Enum.filter(fn x -> x != :__struct__ end)

    processed_map =
      for key <- keys, into: %{} do
        value = Map.get(a_map, key) || Map.get(a_map, to_string(key))
        {key, value}
      end

   Map.merge(a_struct, processed_map)
  end

  def map_from_struct(a_struct) do
    struct_as_map =
      a_struct
        |> Map.from_struct()
        |> Enum.filter(fn {k, v} -> k != :__struct__  && k != :__meta__ end)


    processed_tuple_list = Enum.map(struct_as_map, fn {k, v} ->
      {k, process_value(v)}
    end)

    Enum.into(processed_tuple_list, %{})
  end

  defp process_value(value) when is_list(value) do
     value |> Enum.map(fn v -> process_value(v) end)
  end

  defp process_value(%{__struct__: _} = value) when is_map(value)  do
    Map.from_struct(value) |> Enum.map(fn {k, v} -> {k, process_value(v)} end)
    |> Enum.into(%{})
  end

  defp process_value(value) do
    value
  end

end
