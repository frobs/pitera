defmodule Pitera.Helpers.NumberHelper do
  def to_exact_integer(float) do
    trunc(round(round_to_four_decimals(float) * 10000))
  end

  def to_exact_float(integer) do
    integer / 10000
  end

  def round_to_four_decimals(number) when is_float(number) do
    :erlang.float_to_binary(number, decimals: 4) |> Float.parse |> elem(0)
  end

  def round_to_four_decimals(number) when is_integer(number) do
    :erlang.float_to_binary(number * 1.0, decimals: 4) |> Float.parse |> elem(0)
  end

  def multiply(_number, 0.0), do: 0.0
  def multiply(_number, 0), do: 0.0

  def multiply(float_one, float_two) when is_float(float_one) and is_float(float_two) do
    round_to_four_decimals((to_exact_integer(float_one) * to_exact_integer(float_two)) / 100000000)
  end

  def multiply(number_one, number_two) do
    multiply(round_to_four_decimals(number_one * 1.0), round_to_four_decimals(number_two * 1.0))
  end

  def divide(0, 0), do: 0.0
  def divide(0.0, 0.0), do: 0.0

  def divide(float_one, float_two) when is_float(float_one) and is_float(float_two) do
    round_to_four_decimals((to_exact_integer(float_one) / to_exact_integer(float_two)))
  end

  def divide(number_one, number_two) do
    divide(round_to_four_decimals(number_one * 1.0), round_to_four_decimals(number_two * 1.0))
  end
end
