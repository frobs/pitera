defmodule Pitera.Helpers.DateHelper do
  def get_current_date_as_string() do
    DateTime.utc_now() |> DateTime.to_date() |> Date.to_string()
  end

  def string_to_date(date_as_string) do
    Date.from_iso8601!(date_as_string)
  end
end
