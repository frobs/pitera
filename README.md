# Pitera

This is an example of an inventory management application, made with Phoenix and Elixir.
It is outdated.

## Create database container docker

`docker run --name pitera-db --privileged -p 5432:5432 -e POSTGRES_PASSWORD=1221 -d postgres:11`


## Create cache database

`docker run --name pitera-cache -p 6379:6379 -d redis:5.0.5-alpine`

## Database task

### Create -> `mix ecto.create`
### Drop -> `mix ecto.drop`
### Migrate -> `mix ecto.migrate`

## Phoenix commands

### Install dependencies -> `mix deps.get`
### Run server -> `mix phx.server`
iex -S mix phx.server
### Run console on phoenix app context -> `iex -S mix`