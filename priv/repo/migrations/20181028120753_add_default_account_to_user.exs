defmodule Pitera.Repo.Migrations.AddDefaultAccountToUser do
  use Ecto.Migration
  def change do
    alter table(:users) do
      add :default_account_id, references(:accounts), null: true
    end
  end
end
