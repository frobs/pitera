defmodule Pitera.Repo.Migrations.CreateTags do
  use Ecto.Migration

  def change do
    create table(:tags) do
      add :name, :varchar, size: 200, null: false
      add :normalized_name, :varchar, size: 200, null: false
      add :business_id, references(:business), on_delete: :delete_all
    end

    create unique_index(:tags, [:business_id, :normalized_name], name: :business_normalized_tag_name_index)

 end
end
