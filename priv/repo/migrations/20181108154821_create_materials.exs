defmodule Pitera.Repo.Migrations.CreateMaterials do
  use Ecto.Migration

  def change do
    create table(:materials) do
      add :name, :varchar, size: 200, null: false
      add :sku, :varchar, size: 200, default: ""
      add :annotation, :varchar, size: 400
      add :average_unit_price, :float, null: false
      add :minimun_units, :float, default: 0.0
      add :total_units, :float, null: false
      add :total_price, :float, null: false
      add :properties, :jsonb, default: "{}"
      add :business_id, references(:business), on_delete: :delete_all
      timestamps(type: :utc_datetime)
    end
    create index(:materials, [:business_id, :sku], name: :business_material_sku_index)
    create index(:materials, [:name], name: :material_name_index)
  end
end
