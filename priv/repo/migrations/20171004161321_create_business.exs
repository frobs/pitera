defmodule Pitera.Repo.Migrations.CreateBusiness do
  use Ecto.Migration

  def change do
    create table(:business) do
      add :cif, :varchar, size: 50, null: false, unique: true
      add :name, :varchar, size: 200, null: false, unique: true
      add :email, :varchar, size: 200
      add :phone, :varchar, size: 100
      add :web, :varchar, size: 200
      add :phone_2, :varchar, size: 100
      add :address, :varchar
      add :postal_code, :varchar
      add :town, :varchar
      add :region, :varchar
      add :country, :varchar
      timestamps(type: :utc_datetime)
    end
    create unique_index(:business, [:email])
    create unique_index(:business, [:name])
  end
end
