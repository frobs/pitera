defmodule Pitera.Repo.Migrations.CreateProductsMaterials do
  use Ecto.Migration

  def change do
     create table(:products_materials, primary_key: false) do
      add(:product_id, references(:products, on_delete: :delete_all), primary_key: true)
      add(:material_id, references(:materials, on_delete: :delete_all), primary_key: true)
    end

    create(index(:products_materials, [:product_id]))
    create(index(:products_materials, [:material_id]))

    create(
      unique_index(:products_materials, [:product_id, :material_id], name: :product_id_material_id_unique_index)
    )
 end
end
