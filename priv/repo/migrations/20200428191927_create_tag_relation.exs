defmodule Pitera.Repo.Migrations.CreateTagRelation do
  use Ecto.Migration

  def change do
     create table(:tags_relations, primary_key: false) do
      add(:tag_id, references(:tags, on_delete: :delete_all), primary_key: true)
      add(:material_id, references(:materials, on_delete: :delete_all), primary_key: true)
    end

    create(
      unique_index(:tags_relations, [:tag_id, :material_id], name: :tag_id_material_id_unique_index)
    )
 end
end
