defmodule Pitera.Repo.Migrations.CreateProducts do
  use Ecto.Migration

  def change do
    create table(:products) do
      add :name, :varchar, size: 200, null: false
      add :sku, :varchar, size: 200, default: ""
      add :annotation, :varchar, size: 400
      add :total_units, :float
      add :total_cost, :float
      add :average_unit_cost, :float
      add :profit_percentage, :float
      add :discount, :float, default: 0.0
      add :pvp, :float
      add :minimun_units, :float, default: 0.0
      add :type, :integer
      add :manufacturing_cost, :float, default: 0.0
      add :properties, :jsonb, default: "{}"
      add :business_id, references(:business), on_delete: :delete_all
      timestamps(type: :utc_datetime)
    end
    create unique_index(:products, [:business_id, :sku], name: :business_product_sku_index)
    create index(:products, [:type], name: :product_type_index)
    create index(:products, [:name], name: :product_name_index)
 end
end

