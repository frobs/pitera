defmodule Pitera.Repo.Migrations.CreateStock do
  use Ecto.Migration

  def change do
    create table(:stock) do
      add :units, :float, null: false
      add :unit_price, :float, null: false
      add :total_price, :float, null: false
      add :total_added_cost, :float, null: false, default: 0
      add :total_unit_price, :float, null: false
      add :acquisition_date, :date, null: false
      add :added_costs, :jsonb, default: "[]"
      add :business_id, references(:business), on_delete: :delete_all
      add :material_id, references(:materials), on_delete: :delete_all
      timestamps(type: :utc_datetime)
    end
    create index(:stock, [:material_id])
  end
end
