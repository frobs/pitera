defmodule Pitera.Repo.Migrations.Accounts do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :permissions, :integer
      add :user_id, references(:users)
      add :business_id, references(:business)
    end
    create unique_index(:accounts, [:user_id, :business_id], name: :user_business_index)
  end
end